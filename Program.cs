var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddSession();

var app = builder.Build();

app.UseSession();

app.MapControllerRoute("default", "{controller=User}/{action=Login}/{id?}");

app.Run();
