public class UserRepository : IUserRepository
{
    List<User> users = new List<User> {
        new User {UserId=1, Name="Fulano", Email="fulano@email.com", Password="123"},
        new User {UserId=2, Name="Ciclano", Email="ciclano@email.com", Password="321"}
    };

    public User? Login(string email, string password)
    {
        var user = users.SingleOrDefault(u => u.Email == email && u.Password == password);
        return user;
    }
}