using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

public class UserController : Controller
{
    IUserRepository repository;

    public UserController(IUserRepository repository) 
    {
        this.repository = repository;
    }

    [HttpGet]
    public ActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Login(IFormCollection form)
    {
        string? email = form["email"];
        string? password = form["password"];

        var user = repository.Login(email!, password!);

        if(user == null)
        {
            ViewBag.Error = "Usuário/Senha inválidos";
            return View();
        }

        HttpContext.Session.SetString("user", JsonSerializer.Serialize(user));

        return RedirectToAction("Index", "Home");
    }

    [HttpGet]
    public ActionResult Logout()
    {
        HttpContext.Session.Clear();
        return RedirectToAction("Login");
    }
}